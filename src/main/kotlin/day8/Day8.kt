package day8

import java.io.File

fun main() {
    val lines = File("src/main/resources/day8.txt").readLines()
//    val lines = File("src/main/resources/day8_test.txt").readLines() // Answer 2
//    val lines = File("src/main/resources/day8_test2.txt").readLines() // Answer 6
//    val lines = File("src/main/resources/day8_test3.txt").readLines() // Answer 6

    val instructions = lines[0]

    val regex = """(\w+) = \((\w+), (\w+)\)""".toRegex()
    val nodes = lines.drop(2).map { line ->
        val matchResult = regex.find(line)
        Node(
            label = matchResult?.groups?.get(1)?.value!!,
            left = matchResult.groups[2]?.value!!,
            right = matchResult.groups[3]?.value!!,
        )
    }
    val nodeMap = nodes.associateBy { it.label }

//    part1(instructions, nodeMap)
    part2(instructions, nodeMap)
}

fun part1(instructions: String, nodeMap: Map<NodeLabel, Node>) {
    val numSteps = calNumSteps(instructions, nodeMap, startNode = nodeMap["AAA"]!!, endOnSuffix = "ZZZ")
    println("part 1 - Num Steps: $numSteps")
}

fun part2(instructions: String, nodeMap: Map<NodeLabel, Node>) {
    val startingNodes = nodeMap.values.filter { it.label.endsWith("A") }
    println("Num starting nodes: ${startingNodes.size}")

    val numStepsForEachNode = startingNodes.map { node -> calNumSteps(instructions, nodeMap, node, "Z").toLong() }
    val numStepsForAll = findLCMOfListOfNumbers(numStepsForEachNode)

    println("\n\nPart 2 - num steps for ghosts: $numStepsForAll ")
}

private fun calNumSteps(instructions: String, nodeMap: Map<NodeLabel, Node>, startNode: Node, endOnSuffix: String): Int {
    var currentNode = startNode
    var numSteps = 0
    var i = 0
    while (!currentNode.label.endsWith(endOnSuffix)) {
        if (i >= instructions.length) {
            i = 0
        }

        val instruction = instructions[i]
        val nextNode = if (instruction == 'L') {
            nodeMap[currentNode.left]!!
        } else {
            nodeMap[currentNode.right]!!
        }

//        println("Step $numSteps: ${currentNode.label} + $instruction -> ${nextNode.label}")
        currentNode = nextNode
        i++
        numSteps++
    }
    return numSteps
}

fun findLCM(a: Long, b: Long): Long {
    val larger = if (a > b) a else b
    val maxLcm = a * b
    var lcm = larger
    while (lcm <= maxLcm) {
        if (lcm % a == 0L && lcm % b == 0L) {
            return lcm
        }
        lcm += larger
    }
    return maxLcm
}

fun findLCMOfListOfNumbers(numbers: List<Long>): Long {
    var result = numbers[0]
    for (i in 1 until numbers.size) {
        result = findLCM(result, numbers[i])
    }
    return result
}


typealias NodeLabel = String

data class Node(
    val label: NodeLabel,
    val left: NodeLabel,
    val right: NodeLabel,
) {
    override fun toString(): String = label
}