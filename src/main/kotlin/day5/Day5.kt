package day5

import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.collections.shouldHaveSize
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.system.measureTimeMillis
import kotlin.time.Duration.Companion.milliseconds


fun main() {
//    val fullFile = File("src/main/resources/day5.txt").readText()
    val fullFile = File("src/main/resources/day5_test.txt").readText()

    part1(fullFile)
    part2(fullFile)
}


fun part1(fullFile: String) {
    val seeds = parseSeeds(fullFile)
    val stupidMaps = buildStupidMaps(fullFile)

    val locations = seeds.map { seedToLocation(it, stupidMaps) }
    println("part 1 - Lowest Location: ${locations.minOrNull()}")
}

fun part2(fullFile: String) {
    val stupidMaps = buildStupidMaps(fullFile)
    val seedsLine = parseSeeds(fullFile)

    val runTime = measureTimeMillis {
        val minLocation = seedsLine.asSequence().windowed(2, 2)
            .map { pair -> (pair[0]..<(pair[0] + pair[1])).asSequence() }
            .onEach { range -> println("Calculating min for range [${range.first()}-${range.last()}] (size = ${range.last() - range.first() + 1})") }
            .minOf { range ->
                val minForRange = range.minOf { seed -> seedToLocation(seed, stupidMaps) }
                println("Min for range [${range.first()}-${range.last()}] = $minForRange")
                minForRange
            }

        println("\n\nPart 2 - Lowest Location: $minLocation ")
    }
    println("Run time: ${runTime.milliseconds}")
}

fun buildStupidMaps(fullFile: String): StupidMaps {
    return StupidMaps(
        seedToSoil = buildStupidMap("seed-to-soil", fullFile),
        soilToFertilizer = buildStupidMap("soil-to-fertilizer", fullFile),
        fertilizerToWater = buildStupidMap("fertilizer-to-water", fullFile),
        waterToLight = buildStupidMap("water-to-light", fullFile),
        lightToTemperature = buildStupidMap("light-to-temperature ", fullFile),
        temperatureToHumidity = buildStupidMap("temperature-to-humidity", fullFile),
        humidityToLocation = buildStupidMap("humidity-to-location", fullFile),
    )
}

fun seedToLocation(seed: Long, stupidMaps: StupidMaps): Long {
    val soil = stupidMaps.seedToSoil.map(seed)
    val fertilizer = stupidMaps.soilToFertilizer.map(soil)
    val water = stupidMaps.fertilizerToWater.map(fertilizer)
    val light = stupidMaps.waterToLight.map(water)
    val temperature = stupidMaps.lightToTemperature.map(light)
    val humidity = stupidMaps.temperatureToHumidity.map(temperature)
    val location = stupidMaps.humidityToLocation.map(humidity)

    return location
}

fun StupidMap.map(source: Long): Long {
    return rangeMaps.firstOrNull { it.sourceRangeStart <= source && it.sourceRangeStart + it.rangeLength > source }
        ?.let { it.destRangeStart + (source - it.sourceRangeStart) }
        ?: source // If we don't have a mapping, just return the source
}

data class StupidMaps(
    val seedToSoil: StupidMap,
    val soilToFertilizer: StupidMap,
    val fertilizerToWater: StupidMap,
    val waterToLight: StupidMap,
    val lightToTemperature: StupidMap,
    val temperatureToHumidity: StupidMap,
    val humidityToLocation: StupidMap,
)

data class StupidMap(
    val rangeMaps: List<RangeMap>
)

data class RangeMap(
    val sourceRangeStart: Long,
    val destRangeStart: Long,
    val rangeLength: Long,
    val sourceRangeEnd: Long = sourceRangeStart + rangeLength - 1,
    val offset: Long = destRangeStart - sourceRangeStart
)

private fun parseSeeds(fullFile: String): List<Long> {
    return fullFile.split("\n\n")[0].substringAfter("seeds: ").split(" ").map { it.trim().toLong() }
}

fun buildStupidMap(mapName: String, fullFile: String): StupidMap {
    val seedToSoilMapText = getMapText(mapName, fullFile).split("\n")
    return StupidMap(
        rangeMaps = (1..<seedToSoilMapText.size)
            .map { seedToSoilMapText[it] }
            .map { rangeString ->
                val split = rangeString.split(" ")
                RangeMap(
                    sourceRangeStart = split[1].toLong(),
                    destRangeStart = split[0].toLong(),
                    rangeLength = split[2].toLong(),
                )
            }.toList()
    )
}

fun getMapText(mapName: String, fullFile: String): String {
    return fullFile.split("\n\n").first { it.startsWith(mapName) }.trim()
}

// ---- Everything after here I gave up on ----

fun InputRange.runThroughStupidMap(stupidMap: StupidMap): List<InputRange> {
    val enclosingRanges =
        stupidMap.rangeMaps.filter { rangeMap -> rangeMap.sourceRangeStart <= start && rangeMap.sourceRangeEnd >= end }

    // A single range from the range maps completely encloses the input range
    if (enclosingRanges.size == 1) {
        val offset = enclosingRanges[0].offset
        return listOf(this.copy(start = start + offset, end = end + offset))
    }

    // Other stuff
    return listOf()
}

data class InputRange(
    val start: Long,
    val end: Long,
) {
    fun values(): List<Long> = (start..end).toList()
}

private class Tests {
    @Test
    fun `input range is enc loded in single stupidMap range`() {
        val inputRange = InputRange(start = 51, end = 52) // [51, 52] Size 2
        val stupidMap = StupidMap(
            rangeMaps = listOf(
                RangeMap(sourceRangeStart = 98, destRangeStart = 50, rangeLength = 2),
                RangeMap(sourceRangeStart = 50, destRangeStart = 52, rangeLength = 48),
            )
        )

        val mappedRange = inputRange.runThroughStupidMap(stupidMap)
        mappedRange shouldHaveSize 1
        mappedRange[0].values() shouldContainExactly listOf(53L, 54L)
    }
}

