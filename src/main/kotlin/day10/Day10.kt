package day10

import java.io.File
import kotlin.math.ceil

fun main() {
    val lines = File("src/main/resources/day10.txt").readLines()
//    val lines = File("src/main/resources/day10_test1.txt").readLines()

    part1(lines)
//    part2(lines)
}

typealias Grid = List<List<Pipe>>

fun part1(lines: List<String>) {
    val grid: Grid = parseGrid(lines)
    val startingPipe = grid.flatten().find { it.type == PipeType.STARTING }!!

    // There are two paths found here, right and down. We only need to follow one of them
    val aConnectingPipe = startingPipe.getConnectingPipes(grid).first()

    // This is going both ways through the starting node, I should break once I find a path through
    val longestPath =
        canPathToStart(
            pipe = aConnectingPipe,
            grid = grid,
            inputPipe = startingPipe,
            visited = listOf(startingPipe)
        )

    println("part 1 - Longest path: $longestPath ")
}

/**
 * Recursively trys to find a path to the starting pipe, returning true if it can
 */
tailrec fun canPathToStart(
    pipe: Pipe,
    grid: Grid,
    inputPipe: Pipe,
    visited: List<Pipe>
): Int? {
    println("Looking for path to start depth = ${visited.size} from: $pipe")
    if (pipe.type == PipeType.STARTING) {
        // Round up to nearest int (if it is an odd number, we take the longer route)
        return ceil(visited.size / 2.0).toInt()
    }

    // We are in a loop not to the starting point
    if (pipe in visited) {
        return null
    }

    val outputPipe = pipe.getConnectingPipes(grid).filterNot { it == inputPipe }.first()

    return canPathToStart(
        pipe = outputPipe,
        grid = grid,
        inputPipe = pipe,
        visited = visited + pipe
    )
}

/** returns true if this pipe connects to one below it **/
private fun Pipe?.connectsDown(): Boolean {
    return this?.type in listOf(PipeType.UP_RIGHT, PipeType.UP_LEFT, PipeType.VERTICAL, PipeType.STARTING)
}

private fun Pipe?.connectsUp(): Boolean {
    return this?.type in listOf(PipeType.DOWN_LEFT, PipeType.DOWN_RIGHT, PipeType.VERTICAL, PipeType.STARTING)
}

private fun Pipe?.connectsLeft(): Boolean {
    return this?.type in listOf(PipeType.UP_RIGHT, PipeType.DOWN_RIGHT, PipeType.HORIZONTAL, PipeType.STARTING)
}

private fun Pipe?.connectsRight(): Boolean {
    return this?.type in listOf(PipeType.UP_LEFT, PipeType.DOWN_LEFT, PipeType.HORIZONTAL, PipeType.STARTING)
}

private fun Pipe.getConnectingPipes(grid: Grid): Collection<Pipe> {
    return when (this.type) {
        PipeType.STARTING -> {
            listOfNotNull(
                if (up(grid).connectsDown()) up(grid) else null,
                if (down(grid).connectsUp()) down(grid) else null,
                if (left(grid).connectsLeft()) left(grid) else null,
                if (right(grid).connectsRight()) right(grid) else null,
            )
        }

        PipeType.HORIZONTAL -> listOfNotNull(left(grid), right(grid))
        PipeType.VERTICAL -> listOfNotNull(up(grid), down(grid))
        PipeType.DOWN_RIGHT -> listOfNotNull(up(grid), right(grid))
        PipeType.DOWN_LEFT -> listOfNotNull(up(grid), left(grid))
        PipeType.UP_LEFT -> listOfNotNull(down(grid), left(grid))
        PipeType.UP_RIGHT -> listOfNotNull(down(grid), right(grid))
        PipeType.GROUND -> emptyList()
    }
}

private fun Pipe.up(grid: Grid): Pipe? {
    return if (row == 0) null else grid[row - 1][col]
}

private fun Pipe.down(grid: Grid): Pipe? {
    return if (row == grid.size - 1) null else grid[row + 1][col]
}

private fun Pipe.left(grid: Grid): Pipe? {
    return if (col == 0) null else grid[row][col - 1]
}

private fun Pipe.right(grid: Grid): Pipe? {
    return if (col == grid.size - 1) null else grid[row][col + 1]
}


fun parseGrid(lines: List<String>): Grid {
    val rows = mutableListOf<MutableList<Pipe>>()
    lines.forEachIndexed { rowIdx, line ->
        val row = mutableListOf<Pipe>()
        line.forEachIndexed { colIdx, char ->
            row.add(
                Pipe(
                    row = rowIdx,
                    col = colIdx,
                    symbol = char,
                    type = when (char) {
                        '.' -> PipeType.GROUND
                        '|' -> PipeType.VERTICAL
                        '-' -> PipeType.HORIZONTAL
                        'L' -> PipeType.DOWN_RIGHT
                        'J' -> PipeType.DOWN_LEFT
                        'F' -> PipeType.UP_RIGHT
                        '7' -> PipeType.UP_LEFT
                        'S' -> PipeType.STARTING
                        else -> throw RuntimeException("Unknown char $char")
                    }
                )
            )
        }
        rows.add(row)
    }
    return rows
}

fun part2(lines: List<String>) {
    println("part 2 - ")
}

data class Pipe(
    val symbol: Char,
    val type: PipeType,
    val row: Int,
    val col: Int,
)

enum class PipeType {
    STARTING,
    HORIZONTAL,
    VERTICAL,
    DOWN_RIGHT,
    DOWN_LEFT,
    UP_LEFT,
    UP_RIGHT,
    GROUND,
}
