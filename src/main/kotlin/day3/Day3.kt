package day3

import java.io.File

typealias Line = String

fun main() {
    val lines = File("src/main/resources/day3.txt").readLines()
//    val lines = File("src/main/resources/day3_test.txt").readLines()

    part1(lines)
    part2(lines)
}

data class PartNumber(
    val partNumber: Int,
    val row: Int,
    val startColumn: Int,
    val endColumn: Int
)

data class Gear(
    val row: Int,
    val column: Int,
)

fun part1(lines: List<Line>) {
    val partNumbers = findPartNumbers(lines)
    val filteredPartNumbers = partNumbers.filter { it.isSurroundedBySymbol(lines) }

    println("part 1 - sum of part numbers: ${filteredPartNumbers.sumOf { it.partNumber }}")
}

fun part2(lines: List<Line>) {
    val partNumbers = findPartNumbers(lines)
    val possibleGears = lines.findGears()
    val gearRatioSums = possibleGears.sumOf { it.calculateGearRation(partNumbers) }

    println("Part 2 - sum of gear ratios: $gearRatioSums")
}

// Gears are valid if they touch exactly two parts
private fun Gear.calculateGearRation(parts: List<PartNumber>): Int {
    val touchingParts = parts.filter { part ->
        val touchingLeft = part.row == this.row && part.endColumn == this.column - 1 // Part is left of gear
        val touchingRight = part.row == this.row && part.startColumn == this.column + 1
        val touchingAbove = part.row == this.row - 1  && (part.endColumn >= this.column -1 && part.startColumn <= this.column + 1)
        val touchingBelow = part.row == this.row + 1  && (part.endColumn >= this.column -1 && part.startColumn <= this.column + 1)

        touchingLeft || touchingRight || touchingAbove || touchingBelow
    }

    return if (touchingParts.size == 2) {
        touchingParts[0].partNumber * touchingParts[1].partNumber
    }
    else {
        0 // Not a valid gear
    }
}

private fun List<Line>.findGears(): List<Gear> {
    val gears = mutableListOf<Gear>()
    this.forEachIndexed { row, line ->
        line.forEachIndexed { column, char ->
            if (char == '*') {
                gears.add(Gear(row, column))
            }
        }
    }
    return gears
}

private fun findPartNumbers(lines: List<Line>): MutableList<PartNumber> {
    val partNumbers = mutableListOf<PartNumber>()

    lines.forEachIndexed { lineNumber, line ->
        var i = 0
        while (i < lines.size) {
            val char = line[i]
            if (char.isDigit()) {
                var endIndex = i
                while (endIndex < line.length && line[endIndex].isDigit()) {
                    endIndex++
                }
                val partNumber = PartNumber(
                    partNumber = line.substring(i, endIndex).toInt(),
                    row = lineNumber,
                    startColumn = i,
                    endColumn = endIndex - 1  // We have stepped past the last digit, so subtract 1
                )
                partNumbers.add(partNumber)
                i = endIndex + 1
            } else {
                i++
            }
        }
    }
    return partNumbers
}

private fun PartNumber.isSurroundedBySymbol(lines: List<String>): Boolean {
    val symbolAbove = this.row > 0 && lines[this.row - 1].isAnySymbol(this.startColumn - 1, this.endColumn + 1)
    val symbolBelow = this.row < lines.size - 1 && lines[this.row + 1].isAnySymbol(this.startColumn - 1, this.endColumn + 1)
    val symbolLeft = this.startColumn > 0 && lines[this.row].isSymbol(this.startColumn - 1)
    val symbolRight = this.endColumn < lines[this.row].length - 1 && lines[this.row].isSymbol(this.endColumn + 1)
    return symbolAbove || symbolBelow || symbolLeft || symbolRight
}

private fun Line.isSymbol(column: Int): Boolean {
    return !this[column].isDigit() && this[column] != '.'
}

private fun Line.isAnySymbol(startCol: Int, endCol: Int): Boolean {
    val adjustedStart = kotlin.math.max(startCol, 0)
    val adjustedEnd = kotlin.math.min(endCol, this.length - 1)

    return (adjustedStart..adjustedEnd).any { index ->
        this.isSymbol(index)
    }
}