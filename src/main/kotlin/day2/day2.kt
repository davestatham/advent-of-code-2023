package day2

import java.io.File

fun main(args: Array<String>) {
    val lines = File("src/main/resources/day2.txt").readLines()
//    val lines = File("src/main/resources/day2_test.txt").readLines()

    part1(lines)
    part2(lines)
}

data class Game(
    val id: Int,
    val pulls: List<Pull>
)

data class Pull(
    val red: Int,
    val blue: Int,
    val green: Int,
)

fun part1(lines: List<String>) {
    val maxRed = 12
    val maxGreen = 13
    val maxBlue = 14

    val games = buildGames(lines)
    val possibleGames = games.filter { game ->
        game.pulls.none { pull ->
            pull.red > maxRed || pull.blue > maxBlue || pull.green > maxGreen
        }
    }

    val sum = possibleGames.sumOf { it.id }
    println("Part 1 Sum: $sum")
}

fun part2(lines: List<String>) {
    val games = buildGames(lines)

    val minCubesPerGame = games.map { game ->
        Pull(
            red = game.pulls.maxOf { it.red },
            blue = game.pulls.maxOf { it.blue },
            green = game.pulls.maxOf { it.green },
        )
    }

    val totalPower = minCubesPerGame.sumOf { it.red * it.blue * it.green }
    println("Part 2 Sum of power: $totalPower")
}

private fun buildGames(lines: List<String>): List<Game> {
    return lines.map { line ->
        Game(
            id = line.substring("Game".length, line.indexOf(":")).trim().toInt(),
            pulls = line.substringAfter(":").split(";").map { it.toPull() }
        )
    }
}

private fun String.toPull(): Pull {
    val pulls = this.split(",").map { it.trim() }
    return Pull(
        red = pulls.filter { it.contains("red") }.sumOf { it.split(' ')[0].toInt() },
        blue = pulls.filter { it.contains("blue") }.sumOf { it.split(' ')[0].toInt() },
        green = pulls.filter { it.contains("green") }.sumOf { it.split(' ')[0].toInt() },
    )
}

