package day12

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import java.io.File

val memo = mutableMapOf<Record, Long>()
val debug = false

fun main() {
    val lines = File("src/main/resources/day12.txt").readLines()
//    val lines = File("src/main/resources/day12_test.txt").readLines()

//    part1(lines)
    part2(lines)
}

fun part1(lines: List<String>) {
    val records = lines.map { parseLine(it) }.map { it.removeUnneededOperational() }
    val validArrangements = records.map { it.bruteForceSolution(depth = 0) }

    println("part 1: sum off valid arrangements: ${validArrangements.sum()}")
}

fun part2(lines: List<String>) {
    val records = lines.map { parseLine(it) }
    val expandedRecords = records
        .map { it.expand() } // explode each one out
//        .map { it.removeStartAndEnd()}// Get rid of junk
//        .map { it.removeUnneededOperational()}// Get rid of junk
    val validArrangements = expandedRecords.map { it.bruteForceSolution(depth = 0) }

    println("part 2: sum off valid arrangements: ${validArrangements.sum()}")
}

private fun Record.expand(): Record {
    return Record(
        conditions = (0..<5).map { this.conditions + if (it < 4) listOf(Status.UNKNOWN) else listOf() }.flatten(),
        groupings = (0..<5).map { this.groupings }.flatten(),
    )
}

private fun Record.removeUnneededOperational(): Record {
    return this.copy(conditions = this.conditions.removeUnneededOperational())
}

private fun Record.removeStartAndEnd(): Record {
    return this.copy(conditions = this.conditions.stripPrefix().stripSuffix())
}

// Recursively replaces unknowns readings with both operational and damaged and checks if a valid solution is found.
// Returns the number of possible valid child solutions
private fun Record.bruteForceSolution(depth: Int): Long {
    //Memo it up
    if (memo.contains(this)) {
        return memo[this]!!
    }

    val recordValidity = this.checkValidity()
    return when (recordValidity.validity) {
        Validity.VALID -> {
            if (debug) println("${" ".repeat(depth)}Depth $depth - $this VALID - ${this.groupings}")
            memo[this] = 1
            1
        }

        Validity.INVALID -> {
            if (debug) println("${" ".repeat(depth)}Depth $depth - $this INVALID - ${this.groupings}")
            0
        }

        Validity.UNKNOWN -> {
            if (debug) println("${" ".repeat(depth)}Depth $depth - $this UNKNOWN - ${this.groupings}")
            val firstUnknownIdx = recordValidity.childRecord!!.conditions.indexOfFirst { it == Status.UNKNOWN }
            val operationalChildCount = recordValidity.childRecord.copy(
                conditions = recordValidity.childRecord.conditions.toMutableList().apply {
                    this[firstUnknownIdx] = Status.OPERATIONAL
                }.removeDuplicateOperational()
            )
                .bruteForceSolution(depth + 1)
            val damagedChildCount = recordValidity.childRecord.copy(
                conditions = recordValidity.childRecord.conditions.toMutableList().apply {
                    this[firstUnknownIdx] = Status.DAMAGED
                }.removeDuplicateOperational()
            )
                .bruteForceSolution(depth + 1)

            val myCount = operationalChildCount + damagedChildCount
            memo[this] = myCount
            myCount
        }
    }
}

private fun List<Status>.removeUnneededOperational(): List<Status> {
    return this.stripPrefix().removeDuplicateOperational().stripSuffix()
}

// Removes any prefixed operational readings
private fun List<Status>.stripPrefix(): List<Status> {
    return this.dropWhile { it == Status.OPERATIONAL }
}

private fun List<Status>.stripSuffix(): List<Status> {
    val lastIndex = this.indexOfLast { status -> status != Status.OPERATIONAL }
    return this.subList(0, lastIndex + 1)
}

fun List<Status>.removeDuplicateOperational(): List<Status> {
    if (size < 2) {
        return this
    }
    val tail = drop(1).removeDuplicateOperational()
    return if (first() == Status.OPERATIONAL && tail.first() == Status.OPERATIONAL) {
        tail
    } else {
        (listOf(first()) + tail)
    }
}

private fun parseLine(line: String): Record {
    val splitLine = line.split(" ")
    val readings = splitLine[0].map { char ->
        when (char) {
            '.' -> Status.OPERATIONAL
            '#' -> Status.DAMAGED
            '?' -> Status.UNKNOWN
            else -> throw RuntimeException("Unknown reading for line $line")
        }
    }
    val groupings = splitLine[1].split(",").map { it.toInt() }
    return Record(readings, groupings)
}

data class Record(
    val conditions: List<Status>,
    val groupings: List<Int>,
) {
    override fun toString(): String {
        return conditions.map { it.char }.joinToString("")
    }
}

enum class Validity {
    VALID,
    INVALID,
    UNKNOWN,
}

data class RecordValidity(
    // If validity is unknown, this child record is just the unknown gorups at the end
    val childRecord: Record? = null,
    val validity: Validity,
)

// Returns True if the current record is defiantly invalid, false if it is either valid or unknown
fun Record.checkValidity(): RecordValidity {
    var currentGroupCount = 0
    var currentGroupIdx = -1
    conditions.forEachIndexed { readingIdx, reading ->
        if (reading == Status.UNKNOWN) {
            // We have reached an unknown, this means we don't know this is not valid

            // We can strip off any complete groups we have already seen.  We know  group is
            // complete if the previous thing we saw was an operational pipe
            val groupHasEnded = currentGroupCount == 0
            return if (groupHasEnded) {
                RecordValidity(
                    validity = Validity.UNKNOWN,
                    childRecord = Record(
                        conditions = conditions.drop(readingIdx),
                        groupings = groupings.drop(currentGroupIdx + 1),
                    )
                )
            } else {
                RecordValidity(
                    validity = Validity.UNKNOWN,
                    childRecord = this
                )
            }
        } else if (reading == Status.DAMAGED) {
            // We have entered a new group, up the index
            if (currentGroupCount == 0) currentGroupIdx++

            currentGroupCount++

            if (currentGroupIdx >= groupings.size || currentGroupCount > groupings[currentGroupIdx]) {
                // We have more damaged than we should have
                return RecordValidity(validity = Validity.INVALID)
            }
        } else if (reading == Status.OPERATIONAL) {
            // If we are current counting groups, reset them
            if (currentGroupCount > 0) {
                if (currentGroupCount != groupings[currentGroupIdx]) {
                    // We have a group that is not the correct size
                    return RecordValidity(validity = Validity.INVALID)
                }

                currentGroupCount = 0
            }
        }
    }

    // We are at the end. but there are some groupings we have not found
    if (currentGroupIdx < groupings.size - 1) {
        return RecordValidity(validity = Validity.INVALID)
    }

    return if (currentGroupCount != 0
        && currentGroupCount != groupings[currentGroupIdx]
    ) {
        // We have a group that is not the correct size
        RecordValidity(validity = Validity.INVALID)
    } else {
        // We are at teh end, there are no unknowns, we are valid
        RecordValidity(validity = Validity.VALID)
    }
}

enum class Status(val char: Char) {
    OPERATIONAL('.'),
    DAMAGED('#'),
    UNKNOWN('?'),
}

private class Tests {

    @Test
    fun testExpansion() {
        val expanded = parseLine("???.###????.###????.###????.###????.### 1,1,3,1,1,3,1,1,3,1,1,3,1,1,3")
        val regular = parseLine("???.### 1,1,3")
        expanded shouldBe regular.expand()
    }

    @Test
    fun testExpansion2() {
        val expanded = parseLine(".#?.#?.#?.#?.# 1,1,1,1,1")
        val regular = parseLine(".# 1").expand()
        expanded shouldBe regular
    }

    @Test
    fun `parse line removing unneeded operationals`() {
        parseLine(".... 1,1,3").removeUnneededOperational().conditions shouldBe listOf()
        parseLine(".? 1,1,3").removeUnneededOperational().conditions shouldBe listOf(Status.UNKNOWN)
        parseLine(".?... 1,1,3").removeUnneededOperational().conditions shouldBe listOf(Status.UNKNOWN)
        parseLine(".?.##.. 1,1,3").removeUnneededOperational().conditions shouldBe listOf(
            Status.UNKNOWN,
            Status.OPERATIONAL,
            Status.DAMAGED,
            Status.DAMAGED
        )
        parseLine("?? 1,1,3").removeUnneededOperational().conditions shouldBe listOf(Status.UNKNOWN, Status.UNKNOWN)
        parseLine("?..? 1,1,3").removeUnneededOperational().conditions shouldBe listOf(
            Status.UNKNOWN,
            Status.OPERATIONAL,
            Status.UNKNOWN
        )
        parseLine("?#? 1,1,3").removeUnneededOperational().conditions shouldBe listOf(
            Status.UNKNOWN,
            Status.DAMAGED,
            Status.UNKNOWN
        )
        parseLine("..?#?.. 1,1,3").removeUnneededOperational().conditions shouldBe listOf(
            Status.UNKNOWN,
            Status.DAMAGED,
            Status.UNKNOWN
        )
    }

    @Test
    fun `test line validity`() {
        parseLine("???.### 1,1,3").checkValidity().validity shouldBe Validity.UNKNOWN
        parseLine("#.#.### 1,1,3").checkValidity().validity shouldBe Validity.VALID
        parseLine("##..### 1,1,3").checkValidity().validity shouldBe Validity.INVALID
        parseLine("#.#.### 1,1,3,1").checkValidity().validity shouldBe Validity.INVALID
        parseLine("#.#.### 1,1").checkValidity().validity shouldBe Validity.INVALID
        parseLine("##..??? 1,1,3").checkValidity().validity shouldBe Validity.INVALID

        parseLine("..#...#...###. 1,1,3").checkValidity().validity shouldBe Validity.VALID
        parseLine("..#...#...### 1,1,3").checkValidity().validity shouldBe Validity.VALID
        parseLine("..#...#...## 1,1,3").checkValidity().validity shouldBe Validity.INVALID
        parseLine("..#...#...##. 1,1,3").checkValidity().validity shouldBe Validity.INVALID

        parseLine(".#...#....###. 1,1,3").checkValidity().validity shouldBe Validity.VALID
        parseLine("..#..#....###. 1,1,3").checkValidity().validity shouldBe Validity.VALID

        parseLine("###.##..... 3,2,1").removeUnneededOperational().checkValidity().validity shouldBe Validity.INVALID
        parseLine("###.##..... 3,2,1").checkValidity().validity shouldBe Validity.INVALID

        parseLine("## 1").checkValidity().validity shouldBe Validity.INVALID

        parseLine(".###. 3").checkValidity().validity shouldBe Validity.VALID
        parseLine("#.###. 1,3").checkValidity().validity shouldBe Validity.VALID
        parseLine("#.###.# 1,3,1").checkValidity().validity shouldBe Validity.VALID
        parseLine("##.###.## 2,3,2").checkValidity().validity shouldBe Validity.VALID

        parseLine("#.#?.### 1,1,3").checkValidity().validity shouldBe Validity.UNKNOWN
        parseLine("#.##.### 1,1,3").checkValidity().validity shouldBe Validity.INVALID
        parseLine("#.#..### 1,1,3").checkValidity().validity shouldBe Validity.VALID
    }

    @Test
    fun `test matched groups are removed`() {
        parseLine("???.### 1,1,3").checkValidity().validity shouldBe Validity.UNKNOWN
        parseLine("???.### 1,1,3").checkValidity().childRecord.toString() shouldBe "???.###"
        parseLine("???.### 1,1,3").checkValidity().childRecord!!.groupings shouldBe listOf(1, 1, 3)

        parseLine("#???.### 1,1,3").checkValidity().validity shouldBe Validity.UNKNOWN
        parseLine("#???.### 1,1,3").checkValidity().childRecord.toString() shouldBe "#???.###"
        parseLine("#???.### 1,1,3").checkValidity().childRecord!!.groupings shouldBe listOf(1, 1, 3)

        parseLine(".? 1").checkValidity().validity shouldBe Validity.UNKNOWN
        parseLine(".? 1").checkValidity().childRecord.toString() shouldBe "?"
        parseLine(".? 1").checkValidity().childRecord!!.groupings shouldBe listOf(1)

        parseLine("#.? 1,1").checkValidity().validity shouldBe Validity.UNKNOWN
        parseLine("#.? 1,1").checkValidity().childRecord.toString() shouldBe "?"
        parseLine("#.? 1,1").checkValidity().childRecord!!.groupings shouldBe listOf(1)

        // This group has not ended, so we need to keep it in the child
        parseLine("#? 1").checkValidity().validity shouldBe Validity.UNKNOWN
        parseLine("#? 1").checkValidity().childRecord.toString() shouldBe "#?"
        parseLine("#? 1").checkValidity().childRecord!!.groupings shouldBe listOf(1)

        // Dont strip the group if it is still possibly alive
        parseLine(".#?.### 1,1,3").checkValidity().validity shouldBe Validity.UNKNOWN
        parseLine(".#?.### 1,1,3").checkValidity().childRecord.toString() shouldBe ".#?.###"
        parseLine(".#?.### 1,1,3").checkValidity().childRecord!!.groupings shouldBe listOf(1, 1, 3)

        parseLine("#.#?.### 1,1,3").checkValidity().validity shouldBe Validity.UNKNOWN
        parseLine("#.#?.### 1,1,3").checkValidity().childRecord.toString() shouldBe "#.#?.###"
        parseLine("#.#?.### 1,1,3").checkValidity().childRecord!!.groupings shouldBe listOf(1, 1, 3)
    }
}