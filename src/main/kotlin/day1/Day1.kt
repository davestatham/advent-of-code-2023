package day1

import java.io.File

fun main(args: Array<String>) {
    val lines = File("src/main/resources/day1.txt").readLines()
//    part1()
    part2(lines)
}

fun part1(lines: List<String>) {
    val sum = lines.sumOf { line ->
        val first = line.first { it.isDigit() }.digitToInt()
        val last = line.last { it.isDigit() }.digitToInt()
        (first * 10) + last
    }

    println("Sum: $sum")
}

fun part2(lines: List<String>) {
    val numbers = mapOf(
        "zero" to 0,
        "one" to 1,
        "two" to 2,
        "three" to 3,
        "four" to 4,
        "five" to 5,
        "six" to 6,
        "seven" to 7,
        "eight" to 8,
        "nine" to 9,
        "0" to 0,
        "1" to 1,
        "2" to 2,
        "3" to 3,
        "4" to 4,
        "5" to 5,
        "6" to 6,
        "7" to 7,
        "8" to 8,
        "9" to 9
    )

    var total = 0
    lines.forEach { line ->
        var firstNumber: Int? = null
        var lastNumber: Int? = null
        line.forEachIndexed { i, c ->
            numbers.keys.forEach { numberWeAreLookingFor ->
                if (line.substring(i).startsWith(numberWeAreLookingFor)) {
                    firstNumber = firstNumber ?: numbers[numberWeAreLookingFor]
                    lastNumber = numbers[numberWeAreLookingFor]
                }
            }
        }
        // println("First: $firstNumber, Last: $lastNumber")
        total += (firstNumber!! * 10) + lastNumber!!
    }
    println("\nTotal: $total")
}
