package day6

import java.io.File
import kotlin.system.measureTimeMillis
import kotlin.time.Duration.Companion.milliseconds


fun main() {
    val lines = File("src/main/resources/day6.txt").readLines()
//    val lines = File("src/main/resources/day6_test.txt").readLines()

    part1(lines)
    part2(lines)
}

fun part1(fullFile: List<String>) {
    val timeLine = fullFile[0].split(" ").filter { it.isNotBlank() }.drop(1).map { it.toInt() }
    val distanceLine = fullFile[1].split(" ").filter { it.isNotBlank() }.drop(1).map { it.toInt() }
    val games = timeLine.mapIndexed { index, time -> Game(time, distanceLine[index]) }
    val numWaysToWin = games.map { game ->
        (1..game.time).map { chargeTime ->
            val runTime = game.time - chargeTime
            runTime * chargeTime
        }
            .filter { distanceTravelled -> distanceTravelled > game.topDistance }
            .size
    }

    // Multiply all the ways to win together
    val totalWaysToWin = numWaysToWin.reduce { acc, i -> acc * i }

    println("part 1 - multiple of ways to win: $totalWaysToWin")

}

fun part2(fullFile: List<String>) {
    val time = fullFile[0].split(" ", limit = 2)[1].replace(" ", "").toLong()
    val topDistance = fullFile[1].split(" ", limit = 2)[1].replace(" ", "").toLong()

    val runTime = measureTimeMillis {
        val numWinningOptions = (1..time).asSequence().map { chargeTime ->
            val runTime = time - chargeTime
            runTime * chargeTime
        }
            .filter { distanceTravelled -> distanceTravelled > topDistance }
            .count()
        println("\n\nPart 2 - num winning options: $numWinningOptions ")
    }

    println("Run time: ${runTime.milliseconds}")
}

data class Game(
    val time: Int,
    val topDistance: Int
)