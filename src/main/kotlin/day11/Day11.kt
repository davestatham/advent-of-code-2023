package day11

import java.io.File
import kotlin.math.absoluteValue

fun main() {
    val lines = File("src/main/resources/day11.txt").readLines()
//    val lines = File("src/main/resources/day11_test.txt").readLines()

    part1(lines)
//    part2(lines)
}

typealias Grid = List<List<Char>>

fun part1(lines: List<String>) {
    val gridWithExpandedRows = mutableListOf<List<Char>>()

    // get all the rows in
    lines.map { line ->
        val row = line.map { it }
        gridWithExpandedRows.add(row)

        // If it is all dots, add it again
        if (row.all { it == '.' }) {
            gridWithExpandedRows.add(row)
        }
    }

    // Expand cols
    val gridWithExpandedCols = mutableListOf<MutableList<Char>>()
    repeat((0..<gridWithExpandedRows.size).count()) { gridWithExpandedCols.add(mutableListOf()) }

    for (colIdx in 0..<gridWithExpandedRows[0].size) {
        val col = gridWithExpandedRows.map { row -> row[colIdx] }

        // insert column
        val colShouldExpand = (col.all { it == '.' })
        for (rowIdx in 0..<gridWithExpandedRows.size) {
            gridWithExpandedCols[rowIdx].add(col[rowIdx])

            // Add twice if needed
            if (colShouldExpand) {
                gridWithExpandedCols[rowIdx].add(col[rowIdx])
            }
        }
    }

    printGrid(gridWithExpandedCols)

    // Extract the galaxies from grid
    var galaxyId = 1
    val galaxies = mutableListOf<Galaxy>()
    for (rowIdx in 0..<gridWithExpandedCols.size) {
        for (colIdx in 0..<gridWithExpandedCols[0].size) {
            if (gridWithExpandedCols[rowIdx][colIdx] == '#') {
                galaxies.add(Galaxy(id = galaxyId++, row = rowIdx, col = colIdx))
            }
        }
    }

    var totalDistances = 0
    var numCompared = 1
    for (srcIdx in 0 ..< galaxies.size) {
        for (dstIdx in 0 ..< srcIdx) {
            val src = galaxies[srcIdx]
            val dst = galaxies[dstIdx]
            val numUp = (src.row - dst.row).absoluteValue
            val numAcross = (src.col - dst.col).absoluteValue
            println("${numCompared++}: Comparing ${src.id} to ${dst.id}:  ${numUp + numAcross}")
            totalDistances += numUp + numAcross
        }
    }

    println("part 1 - total path distances: $totalDistances")
}

data class Galaxy(
    val id: Int,
    val row: Int,
    val col: Int,
)

fun printGrid(grid: Grid) {
    grid.forEach { row ->
        row.forEach { col ->
            print(col)
        }
        println()
    }
}
