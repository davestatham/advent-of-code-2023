package day9

import java.io.File

fun main() {
    val lines = File("src/main/resources/day9.txt").readLines()
//    val lines = File("src/main/resources/day9_test.txt").readLines()

    part1(lines)
    part2(lines)
}

fun part1(lines: List<String>) {
    val report = lines.map { it.toHistory() }
    val allNextValues = report.map { it.getNextValue() }
    println("part 1  Sum of next values: ${allNextValues.sum()}")
}

fun part2(lines: List<String>) {
    val report = lines.map { it.toHistory() }
    val allFirstValues = report.map { it.getPreviousValue() }
    println("part 2  Sum of first values: ${allFirstValues.sum()}")
}

private fun String.toHistory(): History {
    return History(
        values = this.split(" ").map { it.toInt() })
}

data class History(
    val values: List<Int>
) {
    fun getNextValue(depth: Int  = 0): Int {
        return if (!isAllZeros()) {
            println("${if (depth != 0) "   " else "" } Depth=$depth: $values")
            val childHistory = buildChileHistory()
            values.last() + childHistory.getNextValue(depth + 1)
        } else {
            0
        }
    }

    fun getPreviousValue(depth: Int  = 0): Int {
        return if (!isAllZeros()) {
            println("${if (depth != 0) "   " else "" } Depth=$depth: $values")
            val childHistory = buildChileHistory()
            values.first() - childHistory.getPreviousValue(depth + 1)
        } else {
            0
        }
    }

    private fun buildChileHistory(): History {
        val childValues = mutableListOf<Int>()
        for (i in 1..<values.size) {
            childValues.add(values[i] - values[i - 1])
        }
        return History(values = childValues)
    }


    private fun isAllZeros(): Boolean {
        return values.all { it == 0 }
    }
}