package day7

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.math.pow

fun main() {
    val lines = File("src/main/resources/day7.txt").readLines()
//    val lines = File("src/main/resources/day7_test.txt").readLines()

    part1(lines)
    part2(lines)
}

fun part1(lines: List<String>) {
    val hands = lines.map { it.toHand(useJokers = false) }
    val sortedHands = hands.sorted()
    val totalWinnings = sortedHands.asReversed().mapIndexed { index, hand -> (index + 1) * hand.bid }.sum()
    println("part 1 - total winnings (no jokers): $totalWinnings")
}

fun part2(lines: List<String>) {
    val hands = lines.map { it.toHand(useJokers = true) }
    val sortedHands = hands.sorted()
    val totalWinnings = sortedHands.asReversed().mapIndexed { index, hand -> (index + 1) * hand.bid }.sum()
    println("part 2 - total winnings (with jokers): $totalWinnings")
}

fun List<Hand>.sorted(): List<Hand> {
    return this.sortedWith(
        compareByDescending<Hand> { it.handType.strength }
            .thenByDescending { it.handOrderScore() }
    )
}

private fun String.toHand(useJokers: Boolean = false): Hand {
    val cards = this.split(" ")[0].map { it.toCard(useJokers) }
    return Hand(
        cards = cards,
        handType = if (useJokers) calculateJokerHandType(cards) else calculateHandType(cards),
        bid = this.split(" ")[1].toInt()
    )
}

private fun calculateHandType(cards: List<Card>): HandType {
    val uniqueCards = cards.toSet()
    return when (uniqueCards.size) {
        1 -> HandType.FIVE_OF_KIND
        2 -> {
            val cardCounts = cards.groupBy { it.value }.mapValues { it.value.size }
            if (cardCounts.values.contains(4)) {
                HandType.FOUR_OF_KIND
            } else {
                HandType.FULL_HOUSE
            }
        }

        3 -> {
            val cardCounts = cards.groupBy { it.value }.mapValues { it.value.size }
            if (cardCounts.values.contains(3)) {
                HandType.THREE_OF_KIND
            } else {
                HandType.TWO_PAIR
            }
        }

        4 -> HandType.ONE_PAIR
        else -> HandType.HIGH_CARD
    }
}


val cardValues = mapOf(
    '2' to 2,
    '3' to 3,
    '4' to 4,
    '5' to 5,
    '6' to 6,
    '7' to 7,
    '8' to 8,
    '9' to 9,
    'T' to 10,
    'J' to 11,
    'Q' to 12,
    'K' to 13,
    'A' to 14
)

private fun Char.toCard(useJokers: Boolean): Card {
    val value = if (useJokers && this == 'J') 1 else cardValues[this]!!
    return Card(
        value = value,
        name = this.toString()
    )
}

data class Hand(
    val cards: List<Card>,
    val handType: HandType,
    val bid: Int,
) {
    fun handOrderScore(): Int {
        return cards.asReversed().mapIndexed { power, card ->
            card.value * (14.0.pow(power)).toInt() // 14 as there are 14 max score for a card
        }.sum()
    }
}

data class Card(
    val value: Int,
    val name: String,

    ) {
    override fun toString(): String {
        return name
    }
}

enum class HandType(val strength: Int) {
    FIVE_OF_KIND(7),
    FOUR_OF_KIND(6),
    FULL_HOUSE(5),
    THREE_OF_KIND(4),
    TWO_PAIR(3),
    ONE_PAIR(2),
    HIGH_CARD(1)
}

data class CardCount(
    val card: Card,
    val count: Int,
)

private fun calculateJokerHandType(cards: List<Card>): HandType {
    val uniqueCards = cards.toSet()
    val joker = Card(1, "J")
    val uniqueNonJokers = uniqueCards.subtract(setOf(joker))

    val numJokers = cards.count { it.name == "J" }

    // Ordered list of most common cards in hand by cardCount
    val numNonJokerCards =
        cards.filterNot { it == joker }.groupBy { it }.mapValues { it.value.size }.map { CardCount(it.key, it.value) }
            .sortedByDescending { it.count }

    // All cards are either the same or a Jack
    if (uniqueNonJokers.size <= 1) {
        return HandType.FIVE_OF_KIND
    } else if (uniqueNonJokers.size == 2) {
        // 4 of a kind if most count of most common cards + #jacks = 4
        return if (numNonJokerCards[0].count + numJokers == 4) {
            HandType.FOUR_OF_KIND
        } else {
            HandType.FULL_HOUSE
        }
    } else if (numNonJokerCards[0].count + numJokers == 3) { // Including jokers, we have three of a kind
        return HandType.THREE_OF_KIND
    }
    // There is at most 1 joker here
    else if (numNonJokerCards[0].count + numNonJokerCards[1].count + numJokers == 4) {
        return HandType.TWO_PAIR
    } else if (numJokers == 1 || numNonJokerCards[0].count == 2) {
        return HandType.ONE_PAIR
    } else {
        return HandType.HIGH_CARD
    }
}

private class Tests {
    @Test
    fun `test hand mappings with Jokers`() {
        "AAAAA 100".toHand(true).handType shouldBe HandType.FIVE_OF_KIND
        "AAAAJ 100".toHand(true).handType shouldBe HandType.FIVE_OF_KIND
        "AAAJJ 100".toHand(true).handType shouldBe HandType.FIVE_OF_KIND
        "AAJJJ 100".toHand(true).handType shouldBe HandType.FIVE_OF_KIND
        "AJJJJ 100".toHand(true).handType shouldBe HandType.FIVE_OF_KIND
        "JJJJJ 100".toHand(true).handType shouldBe HandType.FIVE_OF_KIND

        "23333 100".toHand(true).handType shouldBe HandType.FOUR_OF_KIND
        "2333J 100".toHand(true).handType shouldBe HandType.FOUR_OF_KIND
        "233JJ 100".toHand(true).handType shouldBe HandType.FOUR_OF_KIND
        "23JJJ 100".toHand(true).handType shouldBe HandType.FOUR_OF_KIND

        "AAQQQ 100".toHand(true).handType shouldBe HandType.FULL_HOUSE
        "AAQQJ 100".toHand(true).handType shouldBe HandType.FULL_HOUSE

        "23555 100".toHand(true).handType shouldBe HandType.THREE_OF_KIND
        "23J55 100".toHand(true).handType shouldBe HandType.THREE_OF_KIND
        "23JJ5 100".toHand(true).handType shouldBe HandType.THREE_OF_KIND

        "22334 100".toHand(true).handType shouldBe HandType.TWO_PAIR

        "23455 100".toHand(true).handType shouldBe HandType.ONE_PAIR
        "2345J 100".toHand(true).handType shouldBe HandType.ONE_PAIR

        "23456 100".toHand(true).handType shouldBe HandType.HIGH_CARD
    }

}
