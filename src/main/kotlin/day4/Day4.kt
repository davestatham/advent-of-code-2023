package day4

import java.io.File
import kotlin.math.pow
import kotlin.system.measureTimeMillis
import kotlin.time.Duration.Companion.milliseconds

typealias Line = String

fun main() {
    val lines = File("src/main/resources/day4.txt").readLines()
//    val lines = File("src/main/resources/day4_test.txt").readLines()

//    part1(lines)
    part2(lines)
}

data class Card(
    val id: Int,
    val winningNumbers: Set<Int>,
    val numbersHave: Set<Int>,
)

fun part1(lines: List<Line>) {
    val cards = parseCards(lines)
    val winningNumbers = cards.map { card -> card.calculateWins().size } // Number of winning numbers
    val scores = winningNumbers
        .map { numWinningNumbers ->
            if (numWinningNumbers > 0) {
                2.0.pow(numWinningNumbers.toDouble() - 1)
            } else {
                0.0
            }
        }
        .map { it.toInt() }

    println("part 1 - Sum of Scores: ${scores.sum()}")
}

fun part2(lines: List<Line>) {
    val cards = parseCards(lines)
    val cardsCount = cards.associateWith { 1 }.toMutableMap()

    val runTime = measureTimeMillis {
        for (card in cards) {
            val wins = card.calculateWins().size
            for (j in card.id..<(card.id + wins)) {
                val wonCard = cards[j]
                // We add up the current count for this card (won by previous cards), plus 1 for each time this card[j] is won for the card we are looking at.
                cardsCount[wonCard] = cardsCount[wonCard]!! + cardsCount[card]!!
            }
        }
    }

    println("\n\nPart 2 - Num won cards: ${cardsCount.values.sum()}")
    println("Run time: ${runTime.milliseconds}")
}

fun parseCards(lines: List<Line>): List<Card> {
    return lines.map { line ->
        val cardId = line.substringBefore(":").split(" ").filter { it.isNotBlank() }[1].toInt()
        val splitNumbers = line.substringAfter(":").split("|").toList()
        val winningNumbers = splitNumbers[0].split(" ").filter { it.isNotBlank() }.map { it.trim().toInt() }.toSet()
        val numbersHave = splitNumbers[1].split(" ").filter { it.isNotBlank() }.map { it.trim().toInt() }.toSet()
        Card(
            id = cardId,
            winningNumbers = winningNumbers,
            numbersHave = numbersHave,
        )
    }
}

fun Card.calculateWins(): Set<Int> {
    return this.numbersHave.intersect(this.winningNumbers)
}
